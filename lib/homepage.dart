import 'dart:math';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int num = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ask Me Anything"),
        backgroundColor: Color(0xFF3158AB),
      ),
      body: Container(
        color: Color(0xFF4CA2F5),
        child: Center(
          child: GestureDetector(
            onTap: () {
              setState(() {
                Random random = new Random();
                num = random.nextInt(5) + 1; // from 1 upto 5 included
                print(num);
              });
            },
            child: Image.asset("images/ball$num.png"),
          ),
        ),
      ),
    );
  }
}
